<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Upload d'images</title>
	
	<script src="http://localhost:8080/mvcsample/assets/scripts/jquery-1.9.1.js"></script>
  	<script src="http://localhost:8080/mvcsample/assets/scripts/jquery.form.js"></script>
</head>
<body>
	<!-- Source: http://hmkcode.com/spring-mvc-upload-file-ajax-jquery-formdata/ -->
	<!-- http://www.journaldev.com/2573/spring-mvc-file-upload-example-single-multiple-files -->
	<!-- https://www.mkyong.com/spring-mvc/spring-mvc-file-upload-example/ -->
	<!--  https://www.mkyong.com/spring-mvc/spring-mvc-how-to-include-js-or-css-files-in-a-jsp-page/ -->

	<h1>Uploading File With Ajax</h1><br/>
	<form id="form2" method="post" action="http://localhost:8080/mvcsample/api/upload" enctype="multipart/form-data">
	  <!-- File input -->    
	  <input name="file2" id="file2" type="file" /><br/>
	</form>
	 
	<button value="Submit" onclick="uploadJqueryForm()" >Upload</button><i>Using JQuery Form Plugin</i><br/>
	 
	<div id="result"></div>

	<script type="text/javascript">
		//using jquery.form.js
		function uploadJqueryForm(){
		    $('#result').html('');
		 
		   $("#form2").ajaxForm({
		    success:function(data) { 
		          $('#result').html(data);
		     },
		     dataType:"text"
		   }).submit();
		}
	</script>
</body>
</html>