package net.mse.mvcsample;

import javax.servlet.http.HttpServletRequest;

public class BaseController {
	public String getBaseUrl(HttpServletRequest request) {
        String projectName = request.getContextPath();
        projectName = projectName.replace("/", "");
        
        String baseUrl = String.format("%s://%s:%d/%s/",request.getScheme(),  request.getServerName(), request.getServerPort(), projectName);
        
        return baseUrl;
    }
}
